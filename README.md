# Lokalfuchs CSV Improt
### Ein Python Script zum Import von CSV Dateien in eine MySQL Datenbank

---

To use it, simply:

* in das Verzeichnis: <code>/home/lokalfuchs/csv-import </code> wechseln
* Script ausführen:

```python
python import.py <CSV Datei> <JSON Datei>
```

z.B. für den Branchenimport:

```python
python import.py VI&VA-Branchen.csv data_industry_typ.json
```


---

##### Aufbau der JSON Datei
Die JSON Datei beschreibt, wie und was von der CSV Datei wohin importiert werden soll.

Der Aufbau ist wie folgt:

```json
{
    "sequence": [],
    "tables": [],
    "dict": []
}
```

* **sequence**
  * Tabellen, in der Reihenfolge, in der sie befüllt werden sollen
* **tables**
  * Spalten in der CSV Datei werden der Tabelle zugeordnet, in die sie eingefügt werden soll z.B.
  ```json
  {
    "tables": {"industry_type": ["Branche App", "Branchennr."]},
  }
  ```
* **dict**
  * Name des/ der Attribute(s), welches zur Spalte in der CSV passt z.B.
  ```json
  {
    "dict": {"GP": ["customer_no"], "Kundenname": ["company_name"], "Straße": ["street", "street_no"],
     "Stadt": ["zip_code", "city", "address_suffix"], "Email": ["email"], "Telefon": ["phone_no"],
     "Branche": ["industry_type_id"], "Art": ["industry_type_id"], "Sortiment": ["industry_type_id"]}
  }
  ```