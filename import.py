import mysql.connector
import os
from mysql.connector import errorcode
import itertools
import re
import sys
import json


#testdaten.txt data.json
#VI&VA-Branchen.csv data_industry_typ.json


def getCSVNameByAttributeName(value, list):
    retruns = ()
    for attr in list:
        if list[attr] is not None:
            if value in list[attr]:
                retruns += (attr,)

    if len(retruns) == 1:
        return retruns[0]
    else:
        return retruns


def handleMultipleAttrsInOneField(name, value):
    if name == 'street':
        return re.sub('([0-9]+.{0,1})$', '', value).strip()
    if name == 'street_no':
        m = re.search('([0-9]+.{0,1})$', value)
        return m.group(0)
    if name == 'city':
        temp = re.sub('^([0-9]{5})', '', value).strip()
        return temp.split('/')[0].strip()
    if name == 'zip_code':
        m = re.search('^([0-9]{5})', value)
        return m.group(0)
    if name == 'address_suffix':
        temp = re.sub('^([0-9]{5})', '', value).strip()
        if len(temp.split('/')) == 2:
            return temp.split('/')[1].strip()
        else:
            return ''
    print(name + "is not handled")


def handleNotNullValue(name):
    if name == 'company_type':
        return "'STORE'"
    print(name + "is not handled")


def handleMultipleColToOneField(attr, data, i, name, cursor):
    if name[0] == "industry_type_id":
        key = str(data["Branche"][i]) + '-' + str(data["Art"][i]) + '-' + str(data["Sortiment"][i])
        cursor.execute("SELECT id FROM industry_type WHERE jhi_keys LIKE '%" + key + "%'")
        response = cursor.fetchone()
        if response is not None:
            return response[0]
        else:
            return "NULL"
    return "NULL"


def main():
    if len(sys.argv) != 3:
        # Wird ausgeführt wenn das Programm nicht korrekt aufgerufen wurde
        print("wrong parameters")
        sys.exit()
    else:
        #sys.argv[1] = "VI&VA-Branchen.csv"
        #sys.argv[2] = "data_industry_typ.json"
        csv_path = sys.argv[1]
        with open(sys.argv[2], encoding='utf-8') as f:
            data_json = json.load(f)
        sequence = data_json['sequence']
        tables = data_json['tables']
        dict = data_json['dict']

        importer(csv_path, sequence, tables, dict)


def importer(csv_path, sequence, tables, dict):
    with open(csv_path, newline='', encoding='utf-8') as f:

        rows = [row.strip().split(";") for row in f]
        data = {x[0]: list(x[1:]) for x in zip(*rows)}

        host = 'localhost'

        ids = {}
        try:
            cnx = mysql.connector.connect(host=host, port='3306', user=os.environ['LOKALFUCHS_DB_USER'],
                                          password=os.environ['LOKALFUCHS_DB_PASSWORD'],
                                          database='lokalfuchs')
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
        else:
            print('db connected')

            cursor = cnx.cursor()

            #cursor.execute('select * from company')
            #for a in cursor.fetchall():
            #    print(a)

            for table in sequence:
                print('Import data for: ' + table)
                querySelect = "SELECT id FROM " + table + " WHERE "
                queryInsert = "INSERT INTO " + table + " VALUE "
                attrs = tables[table]
                attrs_names = list(itertools.chain.from_iterable([[] + dict[attr] for attr in attrs]))

                #print(attrs)
                cursor.execute("explain " + table)
                table_attrs = [row for row in cursor]


                for i in range(len(data[attrs[0]])):
                    valuesSelect = []
                    valuesInsert = []

                    for table_attr in table_attrs:
                        if table_attr[0] in attrs_names:
                            attr = getCSVNameByAttributeName(table_attr[0], dict)
                            value = ''
                            if type(attr) == tuple:
                                value = handleMultipleColToOneField(attr, data, i, dict[attr[0]], cursor)
                                if value != 'NULL':
                                    valuesSelect.append(dict[attr[0]][0] + ' = ' + str(value))
                                valuesInsert.append(value)
                            elif len(dict[attr]) > 1:
                                value = handleMultipleAttrsInOneField(table_attr[0], data[attr][i])
                                valuesInsert.append("'" + str(value) + "'")
                                valuesSelect.append(table_attr[0] + " = '" + str(value) + "'")
                            else:
                                value = data[attr][i]
                                valuesInsert.append("'" + str(value) + "'")
                                valuesSelect.append(table_attr[0] + " = '" + str(value) + "'")
                        elif re.match('(.*_id)|(^id$)', table_attr[0]):
                            if table_attr[0] in ids:
                                value = ids[table_attr[0]][i]
                                valuesSelect.append(table_attr[0] + ' = ' + str(value))
                                valuesInsert.append(value)
                            else:
                                valuesInsert.append("NULL")
                        elif table_attr[2] == 'NO':
                            value = handleNotNullValue(table_attr[0])
                            valuesSelect.append(table_attr[0] + ' = ' + str(value))
                            valuesInsert.append(value)
                        else:
                            valuesInsert.append("NULL")
                    cursor.execute(querySelect + " AND ".join(valuesSelect))
                    id = cursor.fetchone();
                    if id is not None:
                        print("row already exists")
                        id = id[0]
                        if ids.get(table + "_id") is None:
                            ids.update({table + "_id": [id]})
                        else:
                            liste = ids.get(table + "_id")
                            liste.append(id)
                            ids.update({table + "_id": liste})
                    else:
                        try:
                            cursor.execute(queryInsert + '(' + ", ".join(str(v) for v in valuesInsert) + ')')
                            cnx.commit()
                        except:
                            print("[ERROR] on " + queryInsert + '(' + ", ".join(str(v) for v in valuesInsert) + ')')
                            cnx.rollback()
                        cursor.execute(querySelect + " AND ".join(valuesSelect))
                        id = cursor.fetchone();
                        if id is not None:
                            print("row inserted")
                            id = id[0]
                            if ids.get(table + "_id") is None:
                                ids.update({table + "_id": [id]})
                            else:
                                liste = ids.get(table + "_id")
                                liste.append(id)
                                ids.update({table + "_id": liste})
            print('finish')
            cursor.close()
            cnx.close()

if __name__ == '__main__':
    main()